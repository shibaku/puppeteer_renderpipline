const axios = require("axios");
const formData = require("form-data");
const fs = require("fs");

const upload = async (filePath, metadata, group, id) => {
  try {
    const data = new formData();
    const url = `http://localhost:8080/api/upload/${group}/${id}`
    data.append("media", fs.createReadStream(filePath));
    data.append("metadata", JSON.stringify(metadata));
    return await axios.post(
      url,
      data,
      {
        maxBodyLength: "Infinity",
        headers: {
          "Content-Type": `multipart/form-data; boundary=${data._boundary}`,
        },
      }
    );
  } catch (e) {
    console.log(e);
  }
};

module.exports = upload;
