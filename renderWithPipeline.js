#!/usr/bin/env node
const render = require("./render.js");
const amqp = require("amqplib");
const fs = require("fs");
const {
  rabbitMQ: { host, port },
} = require("./config.js");

const rabbitHost = process.env?.RABBIT_MQ_HOST
  ? process.env.RABBIT_MQ_HOST
  : host;

async function main() {
  try {
    // !get queue name from config file
    const queueName = "metadata";
    const connection = await amqp.connect(`amqp://${rabbitHost}:${port}`);
    const channel = await connection.createChannel();
    const queue = await channel.assertQueue(queueName, { durable: true });
    await channel.prefetch(1);

    // console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);
    channel.consume(
      queueName,
      async (msg) => {
        console.log("## new message");
        const tokenMetadata = JSON.parse(msg.content);
        // console.log("rendered", config);
        console.log("--- start render ---");
        // ! to avoid reloading the model on every new render set
        // ! wrap the consumer into puppeeterr and only update metadata and rotation angles here
        await render(tokenMetadata);
        console.log("--- render completed ---");
        fs.appendFile(
          "./output.log",
          tokenMetadata["attributes"][0]["value"],
          () => {}
        );
        fs.appendFile("./output.log", "\n\n", () => {});

        channel.ack(msg);
      },
      {
        noAck: false,
      }
    );
  } catch (err) {
    console.error(err);
  }
}
main();
