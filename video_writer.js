var ffmpeg = require("fluent-ffmpeg");

// ffmpeg -r 60 -s 1500x1500 -i ./screenshot-%03d.jpg -i audio2.mp3 -vcodec libx264 -c:a copy -crf 15  -pix_fmt yuv420p test.mp4
function write_mp4(images_path, tokenId) {
  console.time("write_mp4", tokenId);
  console.log("Beginning render of ", images_path);
  const video_path = "./outputs/video/" + tokenId.toString() + ".mp4"
  return new Promise((resolve, reject) => {
    ffmpeg(images_path + "/screenshot-%03d.jpg")
      .inputOptions([
        // "-i ./render-assets/overlay.png", //!overlay
        "-i ./render-assets/shibaku.wav",
        "-pix_fmt yuv420p",
      ])
      .withFpsInput(29.97) // fps matching your jpg or png input sequence
      // .complexFilter(["[2][0]overlay"]) //!overlay
      .videoCodec("libx264")
      // .audioCodec("libmp3lame") // don't set this, as it won't play in Quicktime with this codec
      .size('1080x1080')
      .output(video_path)
      .videoBitrate('3000')
      .on("start", (commandLine) => {
        console.log("**** SPAWN : " + commandLine);
      })
      .on("progress", (progress) => {
        console.log(
          "videoProcessing: " + images_path + "  " + progress.percent + "% done"
        );
      })
      .on("error", (err, stdout, stderr) => {
        console.log("error: " + err.message);
        console.log("stderr:" + stderr);
        return reject(err);
      })
      .on("end", () => {
        console.log("Finished processing images->video", images_path);
        console.timeEnd("write_mp4");
        return resolve(video_path);
      })
      .run();
  });
}

module.exports = write_mp4;
//write_mp4('./data/images')
