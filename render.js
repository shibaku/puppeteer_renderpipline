// const fs = require("fs");
const puppeteer = require("puppeteer");
const fs = require("fs");
const write_mp4 = require("./video_writer");
const uploadIPFS = require('./uploadIPFS')
const { webglRender } = require("./config.js");

const renderSetting = process.env?.WEBGL_RENDER
  ? process.env.WEBGL_RENDER
  : webglRender;

const {
  frontend: { host, port },
  imageQuality,
  viewPort: { width, height },
} = require("./config.js");

const frontendHost = process.env?.FRONTEND_HOST
  ? process.env.FRONTEND_HOST
  : host;

const frontendPort = process.env?.FRONTEND_PORT
  ? process.env.FRONTEND_PORT
  : port;

const URL = `http://${frontendHost}:${frontendPort}/render`;
let page = null;

const initBrowser = async () => {
  console.log("initializing browser");
  const browser = await puppeteer.launch({
    args: [
      `--use-gl=${renderSetting}`,
      "--no-sandbox",
      "--enable-surface-synchronization",
      "--disable-dev-shm-usage",
    ],
  });
  page = await browser.newPage();
  await page.setViewport({ width, height, deviceScaleFactor: 1 });
  await page.goto(URL);
};

// !todo add sub folders for each new render cycle
const render = async (tokenMetadata) => {
  console.time("render");
  console.log(tokenMetadata);
  const jsonMetadata = JSON.stringify(tokenMetadata);
  // console.log({ jsonMetadata });

  if (!page) {
    await initBrowser();
  } else {
    console.log("Browser already initialized");
  }

  const dir = "assets/" + tokenMetadata.name;
  // first check if directory already exists
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
    console.log("Created directory ", dir);
  } else {
    console.log("Directory already exists: ", dir);
  }

  page.on("console", (msg) => console.log("PAGE LOG:", msg.text()));
  // pass and updated metadata

  // await page.evaluate((jsonMetadata) => {
  // document.tokenMetadataJSON = jsonMetadata;
  await page.evaluate((tokenMetadata) => {
    document.tokenMetadata = tokenMetadata;
  }, tokenMetadata);

  await page.waitForSelector("#update-metadata");
  await page.click("#update-metadata");

  // render
  console.log("waiting for model to load");
  await page.waitForSelector("#model-loaded", { timeout: 120000 });
  console.log("start rendering frames");
  let i = 0;
  while (i <= 360) {
    console.log("taking screenshot", dir, i);
    const imgIndex = ("000" + i).slice(-3);
    await page.screenshot({
      fullPage: true,
      path: `${dir}/screenshot-${imgIndex}.jpg`,
      type: "jpeg",
      quality: imageQuality,
    });
    //    console.log("waiting for next frame");
    await page.click("#next-frame");
    await page.waitForSelector("#next-frame");
    i++;
  }
  const tokenId = tokenMetadata["attributes"][1]["value"];
  const video_path = await write_mp4(dir, tokenId);
  console.log({video_path})
  fs.writeFile(
    "./outputs/metadata/" + tokenId + ".json",
    jsonMetadata,
    (err) => {
      if (err) {
        console.error(err)
      } else {
        uploadIPFS(video_path, jsonMetadata, 'shiba', tokenId)
      }
    }
  );
  console.timeEnd("render");
};

module.exports = render;
