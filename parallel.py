import multiprocessing
import subprocess


def f(_):
    subprocess.run(['node', 'renderWithPipeline.js'])

num_processes = 2
pool = multiprocessing.Pool(processes=num_processes)
pool.map(f, range(num_processes))
