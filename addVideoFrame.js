var ffmpeg = require("fluent-ffmpeg");

// ffmpeg -r 60 -s 1500x1500 -i ./screenshot-%03d.jpg -i audio2.mp3 -vcodec libx264 -c:a copy -crf 15  -pix_fmt yuv420p test.mp4
function addFrame(images_path, tokenId) {
  ffmpeg()
    .input("./assets/4.mp4")
    .input("./assets/frame.png")
    .videoCodec("libx264")
    .outputOptions("-pix_fmt yuv420p")
    .complexFilter(["[0:v] [1:v] overlay=0:0"])
    .output("./assets/frame.mp4")
    .run();
}
addFrame();

module.exports = addFrame;
